#!/usr/bin/python

out = ''

content = open('atomPositions.csv', 'rb')
for line in content:
	linecontent = line.strip().split(',')
	elements = [str(float(element) / 2.) for element in linecontent]
	out += '%s\n' % (','.join(elements))
content.close()

content = open('atomPositions.csv', 'w')
content.write(out)
content.close()